ARG REGISTRY
###Builder Image###
# FROM ${REGISTRY}/maven as builder
# LABEL maintainer="Aung Myat Kyaw <aungmyatkyaw.kk@gmail.com>"

# ARG JAR_DIR

# ## Git CI Variables ##
# ARG CI_JOB_TOKEN
# ARG CI_API_V4_URL
# ARG CI_PROJECT_ID

# WORKDIR /opt/app/

# COPY . .

# RUN mvn deploy -s ./settings.xml
# RUN mvn clean install -s ./settings.xml
# RUN mv ${PRJ_NAME}target/*.jar app.jar

###Executable Image###
FROM ${REGISTRY}/base-images/openjdk
COPY target/*.jar app.jar
VOLUME /config
